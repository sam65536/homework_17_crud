<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<br/>
<a href="createUser">Create new user</a><br/><br/>
<c:if test="${page.items.size() > 0}">
<table>
    <tr>
        <th>ID</th>
        <th>FIRST NAME</th>
        <th>LAST NAME</th>
        <th>AGE</th>
        <th colspan="3">ACTION</th>
    </tr>
    <c:forEach items="${page.items}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.firstName}</td>
            <td>${user.lastName}</td>
            <td>${user.age}</td>
            <td><a href="showUser?id=${user.id}">show</a></td>
            <td><a href="updateUser?id=${user.id}">edit</a></td>
            <td><a href="deleteUser?id=${user.id}">delete</a></td>
        </tr>
    </c:forEach>
</table>
</c:if>
<br/>
<c:if test="${page.pageCount > 1}">
<table>
    <tr>
        <c:if test="${page.page != 1}">
            <td><a href="users?page=${page.page - 1}">previous</a></td>
        </c:if>
        <c:forEach begin="1" end="${page.pageCount}" var="i">
            <c:choose>
                <c:when test="${page.page eq i}">
                    <td>${i}</td>
                </c:when>
            <c:otherwise>
                <td><a href="users?page=${i}">${i}</a></td>
            </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${page.page lt page.pageCount}">
            <td><a href="users?page=${page.page + 1}">next</a></td>
        </c:if>
    </tr>
</table>
</c:if>
</body>
</html>