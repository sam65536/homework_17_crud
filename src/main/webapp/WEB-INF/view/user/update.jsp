<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<form method="POST" action="updateUser?id=${id}">
    <table>
        <tr><td>First name</td><td><input type="text" name="firstName" value="${firstName}"/></td></tr>
        <tr><td>Last name</td><td><input type="text" name="lastName" value="${lastName}"/></td></tr>
        <tr><td>Age</td><td><input type="text" name="age" value="${age}"/></td></tr>
        <tr><td colspan="5"><input type="submit" value="Save"/></td></tr>
    </table>
</form>
</body>
</html>