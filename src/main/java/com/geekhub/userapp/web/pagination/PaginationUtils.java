package com.geekhub.userapp.web.pagination;

import java.util.List;

public class PaginationUtils {

    private PaginationUtils() {
    }

    public static <T> Page<T> getPage(List<T> items, PageRequest pageRequest) {
        int page = pageRequest.getPage();
        int pageSize = pageRequest.getPerPage();
        int pageCount = (items.size() - 1)/pageSize + 1;
        int itemsCount = Math.min(pageSize, items.size() - (page - 1)*pageSize);
        List<T> pageItems = items.subList((page - 1)*pageSize, (page - 1)*pageSize + itemsCount);
        return new Page(page, pageCount, pageItems);
    }
}