package com.geekhub.userapp.web.controller;

import com.geekhub.userapp.repository.UserRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "deleteUser", urlPatterns = "/deleteUser")
public class UserDeleteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        ServletContext context = getServletContext();
        UserRepository userRepository = (UserRepository) context.getAttribute("userRepository");
        userRepository.delete(id);
        context.setAttribute("userRepository", userRepository);
        response.sendRedirect("/users");
    }
}