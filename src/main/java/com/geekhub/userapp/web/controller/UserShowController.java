package com.geekhub.userapp.web.controller;

import com.geekhub.userapp.model.User;
import com.geekhub.userapp.repository.UserRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "showUser", urlPatterns = "/showUser")
public class UserShowController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        ServletContext context = getServletContext();
        UserRepository userRepository = (UserRepository) context.getAttribute("userRepository");
        User user = userRepository.find(id);
        request.setAttribute("id", user.getId());
        request.setAttribute("firstName", user.getFirstName());
        request.setAttribute("lastName", user.getLastName());
        request.setAttribute("age", user.getAge());
        request.getRequestDispatcher("/WEB-INF/view/user/show.jsp").forward(request, response);
    }
}