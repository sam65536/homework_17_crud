package com.geekhub.userapp.web.controller;

import com.geekhub.userapp.repository.UserRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        UserRepository userRepository = (UserRepository) context.getAttribute("userRepository");
        context.setAttribute("userRepository", userRepository);
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}