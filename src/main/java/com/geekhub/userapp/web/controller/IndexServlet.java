package com.geekhub.userapp.web.controller;

import com.geekhub.userapp.repository.UserRepositoryImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Index", urlPatterns = "/")
public class IndexServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = getServletContext();
        context.setAttribute("userRepository", new UserRepositoryImpl());
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }
}